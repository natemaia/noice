/* See LICENSE file for copyright and license details. */

/* comment to use sendfile if available
 * ~10% faster but, linux specific and disables loading bar */
#define USE_LOADING

/* advance the cursor automatically after select */
#define SEL_ADVANCE

const char *cursor    = " > "; /* indicator for the currently selected entry */
const char *nocursor  = "   "; /* spacer to align (or not) the selected entry and the rest */
const char *yanksym   = "* ";  /* indicator for what files are currently yanked */

static int dirsort    = 0;     /* sort by grouping directories first */
static int sizesort   = 0;     /* sort by size (directories will always be 4 KB) */
static int timesort   = 0;     /* sort by last modified time */
static int verssort   = 1;     /* sort by version number */

static int tildehome  = 0;     /* draw a tilde (~) for $HOME instead of the full path */
static int usecolour  = 1;     /* use colour to better distinguish between filetypes */
static int showsize   = 0;     /* show the file size (doesn't affect sizesort) */
static int showhidden = 0;     /* show hidden files begging with a period "." */

/* filetype colour, symbol, and mask */
struct filetype filetypes[] = {
	/* symbol,   filetype,        colour mask */
	{ '/',       S_IFDIR,     A_NORMAL | COLOR_PAIR(4) }, /* directory */
	{ '@',       S_IFLNK,     A_NORMAL | COLOR_PAIR(6) }, /* symlink */
	{ '=',       S_IFSOCK,    A_NORMAL | COLOR_PAIR(5) }, /* socket */
	{ '|',       S_IFIFO,     A_NORMAL | COLOR_PAIR(5) }, /* fifo */
	{ '#',       S_IFBLK,     A_NORMAL | COLOR_PAIR(3) }, /* block */
	{ '*',       S_IXUSR,     A_NORMAL | COLOR_PAIR(2) }, /* exec */
	/* when non match we consider it a normal file and no highlighting is used */
};

/* jump characters and matching path to jump to for use with g and ' binds */
struct dirjump dirjumps[] = {
	/* key,    path */
	{ 'r',     "/"         },
	{ 'e',     "/etc"      },
	{ 'b',     "/bin"      },
	{ 'u',     "/usr"      },
	{ 'm',     "/media"    },
	{ '.',     "~/.config" },
	{ '\'',    lastdir     }, /* lastdir will be the last directory before jumping */
};

/* file opening application matching */
struct filerule filerules[] = {
	/*  filename regex,                         command,        compiled regex */
	{ "\\.(avi|mp4|mkv|mp3|ogg|flac|mov|wav)$", "ffplay",       { 0 } },
	{ "\\.(xz|gz|tar|zip|7z|rar|deb|bz2)$",     "aunpack",      { 0 } },
	{ "\\.(png|jpg|jpeg|gif)$",                 "sxiv -a &",    { 0 } }, /* flag and background with '&' at the end */
	{ "\\.(html|svg)$",                         "firefox",      { 0 } },
	{ "\\.(iso|img)$",                          "st -e flash",  { 0 } },
	{ "\\.pdf$",                                "mupdf &",      { 0 } },
	{ ".",                                      "less",         { 0 } },
};

/* associate keys with an action */
struct keybind keybinds[] = {
	/* key,            action,     fallback,     environment var */
	{ 'q',             SEL_QUIT,    NULL,            NULL      }, /* exit noice */
	{ 'h',             SEL_BACK,    NULL,            NULL      }, /* go back up the directory tree */
	{ KEY_LEFT,        SEL_BACK,    NULL,            NULL      }, /* ^^^ */
	{ KEY_BACKSPACE,   SEL_BACK,    NULL,            NULL      }, /* ^^^ */
	{ 'l',             SEL_GOIN,    NULL,            NULL      }, /* descend into directory or try to open file */
	{ KEY_RIGHT,       SEL_GOIN,    NULL,            NULL      }, /* ^^^ */
	{ KEY_ENTER,       SEL_GOIN,    NULL,            NULL      }, /* ^^^ */
	{ '\r',            SEL_GOIN,    NULL,            NULL      }, /* ^^^ */
	{ '/',             SEL_FLTR,    NULL,            NULL      }, /* change filter applied to the directory listing */
	{ 'j',             SEL_NEXT,    NULL,            NULL      }, /* move to the next entry */
	{ KEY_DOWN,        SEL_NEXT,    NULL,            NULL      }, /* ^^^ */
	{ 'k',             SEL_PREV,    NULL,            NULL      }, /* move to the previous entry */
	{ KEY_UP,          SEL_PREV,    NULL,            NULL      }, /* ^^^ */
	{ CONTROL('D'),    SEL_PGDN,    NULL,            NULL      }, /* go down one page (terminal lines) */
	{ KEY_NPAGE,       SEL_PGDN,    NULL,            NULL      }, /* ^^^ */
	{ CONTROL('U'),    SEL_PGUP,    NULL,            NULL      }, /* go up one page (terminal lines) */
	{ KEY_PPAGE,       SEL_PGUP,    NULL,            NULL      }, /* ^^^ */
	{ 'G',             SEL_LAST,    NULL,            NULL      }, /* go to the last entry */
	{ KEY_END,         SEL_LAST,    NULL,            NULL      }, /* ^^^ */
	{ KEY_HOME,        SEL_FIRST,   NULL,            NULL      }, /* go to the first entry, 'gg' also works */
	{ 'g',             SEL_JUMP,    NULL,            NULL      }, /* prompt for a jump key */
	{ '\'',            SEL_JUMP,    NULL,            NULL      }, /* ^^^ */
	{ ' ',             SEL_MARK,    NULL,            NULL      }, /* toggle the marked state of the current entry */
	{ 'y',             SEL_YANK,    NULL,            NULL      }, /* yanked marked entries or the current when none are marked */
	{ 'u',             SEL_UNYANK,  NULL,            NULL      }, /* un-yank previously yanked entries */
	{ 'D',             SEL_RM,      NULL,            NULL      }, /* prompt to remove marked entries or the current when none are marked */
	{ 'L',             SEL_LN,      NULL,            NULL      }, /* create link(s) to the last yanked entries */
	{ 'p',             SEL_CP,      NULL,            NULL      }, /* copy last yanked entries to the current directory */
	{ 'm',             SEL_MV,      NULL,            NULL      }, /* move last yanked entries to the current directory */
	{ 'c',             SEL_CD,      NULL,            NULL      }, /* change directory */
	{ 'n',             SEL_NEW,     NULL,            NULL      }, /* prompt to create a new file or directory, nested paths are supported */
	{ 'd',             SEL_DSORT,   NULL,            NULL      }, /* toggle sorting by directories first */
	{ 'v',             SEL_VERS,    NULL,            NULL      }, /* toggle sorting by version number */
	{ 't',             SEL_MTIME,   NULL,            NULL      }, /* toggle sorting by last modified time */
	{ 's',             SEL_SIZE,    NULL,            NULL      }, /* toggle sorting by file size */
	{ 'S',             SEL_SSIZE,   NULL,            NULL      }, /* toggle showing file size */
	{ CONTROL('H'),    SEL_DOTS,    NULL,            NULL      }, /* toggle showing hidden files */
	{ '.',             SEL_DOTS,    NULL,            NULL      }, /* ^^^ */
	{ CONTROL('L'),    SEL_REDRAW,  NULL,            NULL      }, /* force a redraw */
	{ 'r',             SEL_RENAME, "vi",            "EDITOR"   }, /* rename marked entries with EDITOR or vi if empty */
	{ '!',             SEL_RUN,    "sh",            "SHELL"    }, /* run SHELL or sh if empty */
	{ 'z',             SEL_RUN,    "top",           "NOICETOP" }, /* run NOICETOP or top if empty */
	{ '?',             SEL_RUN,    "man noice",     "NOICEMAN" }, /* run NOICEMAN or man noice if empty */
	{ 'e',             SEL_RUNARG, "vi",            "EDITOR"   }, /* open the current file with EDITOR or vi if empty */
	{ 'M',             SEL_RUNARG, "mpv --shuffle", "NOICEMP"  }, /* open the current file with NOICEMP or mpv if empty */
};
