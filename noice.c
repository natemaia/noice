/* See LICENSE file for copyright and license details. */

#define _GNU_SOURCE
#define __USE_XOPEN2K8
#define _XOPEN_SOURCE 700
#define _XOPEN_SOURCE_EXTENDED 1

#include <sys/stat.h>
#include <sys/wait.h>

#if defined(__linux__)
#include <sys/inotify.h>

#define WATCHMASK   IN_CREATE     | IN_DELETE   | IN_DELETE_SELF \
                  | IN_ATTRIB     | IN_MODIFY   | IN_MOVE_SELF   \
                  | IN_MOVED_FROM | IN_MOVED_TO

#ifndef USE_LOADING
#include <sys/sendfile.h>
#endif

#ifndef NCURSES_WIDECHAR
#define NCURSES_WIDECHAR 1
#endif

#elif defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__) || defined(__APPLE__)
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#define WATCHMASK   NOTE_DELETE | NOTE_EXTEND | NOTE_LINK \
                  | NOTE_RENAME | NOTE_REVOKE | NOTE_WRITE
#endif

#include <curses.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <locale.h>
#include <regex.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <ctype.h>
#include <wchar.h>
#include <err.h>

#include "arg.h"

#define ISODD(x)   ((x) & 1)
#define CONTROL(c) ((c) ^ 0x40)
#define META(c)    ((c) ^ 0x80)
#define MIN(x, y)  ((x) < (y) ? (x) : (y))
#define MAX(x, y)  ((x) > (y) ? (x) : (y))
#define LEN(x)     (sizeof(x) / sizeof(*(x)))
#define DOT(d)     (d[0] == '.' && (d[1] == '\0' || (d[1] == '.' && d[2] == '\0')))

#define KEY_DEL    0177
#define KEY_ESC    033
#define KEY_BS     010
#define KEY_RETURN 015

#define PRINTVA()                   \
		move(LINES - 1, 0);         \
		clrtoeol();                 \
		va_start(ap, fmt);          \
		vw_printw(stdscr, fmt, ap); \
		va_end(ap)

enum actions {
	/* these at the top are used for indexing a config array */
	SEL_DSORT = 1, SEL_MTIME, SEL_SIZE, SEL_SSIZE, SEL_VERS,
	SEL_QUIT, SEL_BACK, SEL_GOIN, SEL_FLTR, SEL_NEXT, SEL_PREV,
	SEL_PGDN, SEL_PGUP, SEL_LAST, SEL_FIRST, SEL_JUMP, SEL_MARK,
	SEL_YANK, SEL_UNYANK, SEL_RM, SEL_LN, SEL_CP, SEL_MV, SEL_CD,
	SEL_NEW, SEL_RENAME, SEL_REDRAW, SEL_RUN, SEL_RUNARG, SEL_DOTS,
};

enum recursion_type {
	DEPTH = 0,
	BREADTH = 1,
};

typedef struct entry entry;
typedef struct dirjump dirjump;
typedef struct keybind keybind;
typedef struct filetype filetype;
typedef struct filerule filerule;
typedef struct recursor recursor;
typedef struct colourpair colourpair;
typedef struct savedentry savedentry;


struct recursor {
	const char *src, *dst;
	int slen, dlen, depth;
	enum recursion_type type;
	int (*fn)(char *, char *, struct stat *);
};

struct keybind {
	int sym;
	enum actions act;
	char *run, *env;
};

struct entry {
	int marked;
	mode_t mode;
	time_t mtime;
	long long size;
	char name[NAME_MAX];
};

struct dirjump {
	int key;
	char *path;
};

struct filetype {
	int idchar;
	unsigned int ftmask, colourmask;
};

struct filerule {
	char *regex, *argv;
	regex_t regcomp;
};

struct colourpair {
	int fg, bg;
};

struct savedentry {
	mode_t mode;
	char name[NAME_MAX];
};

colourpair colours[] = {
	{ .fg = 0, .bg = 0 }, /* pairs start at 1 */
	{ COLOR_RED,  -1 }, { COLOR_GREEN,   -1 }, { COLOR_YELLOW, -1 },
	{ COLOR_BLUE, -1 }, { COLOR_MAGENTA, -1 }, { COLOR_CYAN,   -1 },
};

uid_t uid;
entry *dents;
savedentry *sdents;
char tempfile[] = "/tmp/noice.XXXXXX";
char *savefile = "/tmp/noicedir", *home = NULL;
int ndents, nsdents, cur, conf[6], nmarked = 0, longest = 0;
char *argv0, hist[3][PATH_MAX], lastdir[PATH_MAX], msg[LINE_MAX], sdpath[PATH_MAX];
int watcher, watch = -1;

#if defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__) || defined(__APPLE__)
struct kevent kev;
struct timespec kevt;
#endif

#include "noice.h"

void info(char *fmt, ...)
{
	va_list ap;

	PRINTVA();
}

void xwarn(char *fmt, ...)
{
	va_list ap;

	PRINTVA();
	printw(": %s\n", strerror(errno));
}

void fatal(char *fmt, ...)
{
	va_list ap;

	endwin();
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, ": %s\n", strerror(errno));
	va_end(ap);
	exit(1);
}

size_t strlcat(char *dst, const char *src, size_t size)
{
	size_t n = size, dlen;
	const char *odst = dst;
	const char *osrc = src;

	while (n-- != 0 && *dst != '\0')
		dst++;
	dlen = dst - odst;
	n = size - dlen;

	if (n-- == 0)
		return dlen + strlen(src);
	while (*src != '\0') {
		if (n != 0) {
			*dst++ = *src;
			n--;
		}
		src++;
	}
	*dst = '\0';

	return dlen + (src - osrc);
}

size_t strlcpy(char *dst, const char *src, size_t size)
{
	size_t n = size;
	const char *osrc = src;

	if (n != 0)
		while (--n != 0)
			if ((*dst++ = *src++) == '\0')
				break;
	if (n == 0) {
		if (size != 0)
			*dst = '\0';
		while (*src++);
	}
	return src - osrc - 1;
}

int strvcmp(const char *str1, const char *str2)
{
	size_t i1 = 0, i2 = 0;
	size_t len1 = strlen(str1), len2 = strlen(str2);

	for (; i1 < len1 && i2 < len2; i1++, i2++) {
		unsigned char c1 = str1[i1], c2 = str2[i2];
		if (isdigit(c1) && isdigit(c2)) {
			unsigned long long int num1, num2;
			char *end1, *end2;
			num1 = strtoull(str1 + i1, &end1, 10);
			num2 = strtoull(str2 + i2, &end2, 10);
			if (num1 < num2) return -1;
			if (num1 > num2) return 1;
			i1 = end1 - str1 - 1;
			i2 = end2 - str2 - 1;
			if (i1 < i2) return -1;
			if (i1 > i2) return 1;
		} else {
			if (tolower(c1) < tolower(c2)) return -1;
			if (tolower(c1) > tolower(c2)) return 1;
		}
	}
	return (len1 < len2) ? -1 : (len1 > len2) ? 1 : 0;
}

int freesdents()
{
	if (!nsdents) return -1;
	free(sdents);
	*sdpath = '\0';
	sdents = NULL;
	return (nsdents = 0);
}

void initrules()
{
	for (unsigned int i = 0; i < LEN(filerules); i++) {
		int r;
		if ((r = regcomp(&filerules[i].regcomp, filerules[i].regex, REG_NOSUB|REG_EXTENDED|REG_ICASE)) != 0) {
			char errbuf[NAME_MAX];
			regerror(r, &filerules[i].regcomp, errbuf, sizeof(errbuf));
			fprintf(stderr, "invalid regex filerules[%u]: %s: %s\n", i, filerules[i].regex, errbuf);
			exit(1);
		}
	}
}

void initcolour(void)
{
	if (!usecolour || !has_colors()) return;
	start_color();
	use_default_colors();
	for (unsigned int i = 1; i < LEN(colours); i++)
		init_pair(i, colours[i].fg, colours[i].bg);
}

void initcurses(void)
{
	initscr();
	initcolour();
	cbreak();
	noecho();
	nonl();
	intrflush(stdscr, FALSE);
	keypad(stdscr, TRUE);
	curs_set(FALSE); /* hide cursor */
	timeout(1000); /* one second */
}

void *xcalloc(size_t nmemb, size_t size)
{
	void *p;

	if ((p = calloc(nmemb, size)) == NULL) fatal("calloc");
	return p;
}

void *xmalloc(size_t size)
{
	void *p;

	if ((p = malloc(size)) == NULL) fatal("malloc");
	return p;
}

void *xrealloc(void *p, size_t size)
{
	void *tmp = p;
	if (!(tmp = realloc(tmp, size))) fatal("realloc");
	p = tmp;
	return p;
}

int xgetch(char *prompt)
{
	int c, oesc = ESCDELAY;
	wint_t ch = ERR;

	ESCDELAY = 100;
	if (prompt) {
		info("%s", prompt);
		c = get_wch(&ch);
		move(LINES - 1, 0);
		clrtoeol();
	} else {
		c = get_wch(&ch);
	}
	ESCDELAY = oesc;
	if (c != ERR) return ch;
	return ERR;
}

int confirm(char *file, int chkdir, char *fmt, ...)
{
	struct stat st;

	if (!stat(file, &st) && (!chkdir || !S_ISDIR(st.st_mode))) {
		va_list ap;
		PRINTVA();
		timeout(-1);
		char c = xgetch(NULL);
		timeout(1000);
		if (c != 'y' && c != 'Y') return 0;
	}
	return 1;
}

char *readln(char *prompt, const char *initial)
{
	wchar_t *line;
	static char out[LINE_MAX];
	int n, c, max, ilen = 0, plen = strlen(prompt);
	
	line = calloc(LINE_MAX, sizeof(wchar_t));
	if (initial) ilen = mbstowcs(line, initial, LINE_MAX);

	info("%s", prompt);
	mvaddwstr(LINES - 1, plen, line);
	n = max = ilen;
	timeout(-1);
	curs_set(TRUE);
	keypad(stdscr, 1);
	while ((c = xgetch(NULL)) != ERR && max < LINE_MAX - 1) {
		switch (c) {
		case KEY_EOL:    /* fallthrough */
		case KEY_RETURN: /* fallthrough */
		case KEY_ENTER:
			goto out;
		case KEY_ESC:
			line[0] = L'\0';
			goto out;
		case KEY_LEFT:
			if (n) n--;
			break;
		case KEY_RIGHT:
			if (n < max) n++;
			break;
		case KEY_DEL: /* fallthrough */
		case KEY_BS:  /* fallthrough */
		case KEY_BACKSPACE:
			if (!n || !max) break;
			if (line[n] != L'\0')
				memmove(line + n - 1, line + n, (max - n + 1) * sizeof(wchar_t)); // NOLINT
			else
				line[n - 1] = L'\0';
			n--, max--;
			break;
		default:
			if (!isprint(c)) break;
			if (line[n] != L'\0')
				memmove(line + n + 1, line + n, (max - n + 1) * sizeof(wchar_t)); // NOLINT
			line[n++] = c;
			max++;
		}
		move(LINES - 1, plen);
		clrtoeol();
		mvaddwstr(LINES - 1, plen, line);
		move(LINES - 1, plen + n);
	}
out:
	move(LINES - 1, 0);
	clrtoeol();
	line[max] = L'\0';
	curs_set(FALSE);
	timeout(1000);
	memset(out, 0, sizeof(out)); // NOLINT
	if (wcstombs(out, line, LINE_MAX - 1) >= LINE_MAX - 1)
		out[LINE_MAX - 1] = '\0';
	free(line);
	return out[0] ? out : NULL;
}

char *xgetenv(char *name, char *fallback)
{
	static char *value;

	if (!name) return fallback;
	return (value = getenv(name)) && value[0] ? value : fallback;
}

char *mkpath(char *path, char *name, char *out, size_t n)
{
	if (name[0] == '/') {
		strlcpy(out, name, n); /* absolute path */
	} else {
		if (path[0] == '/' && path[1] == '\0') { /* root case */
			strlcpy(out, "/", n);
		} else {
			strlcpy(out, path, n);
			strlcat(out, "/", n);
		}
		strlcat(out, name, n);
	}
	return out;
}

void savecur(char *path, int cur, int histidx, int walk)
{
	int i = cur;

	if (ndents) {
		if (walk) {
			if (cur == ndents - 1) {
				for (; i > 0 && dents[i].marked; i--);
				if (i == 0 && dents[i].marked)
					for (i = cur; i < ndents && dents[i].marked; i++);
				if (i == ndents) i = 0;
			} else {
				for (; i < ndents && dents[i].marked; i++);
				if (i == ndents && dents[i].marked)
					for (i = cur; i > 0 && dents[i].marked; i--);
			}
		}
		mkpath(path, dents[i].name, hist[histidx], sizeof(hist[histidx]));
	}
}

int spawnvp(char *dir, char *argv[], int bg)
{
	pid_t pid;
	int status, r = 0;

	endwin();
	switch ((pid = fork())) {
	case -1:
		r = -1;
		break;
	case 0:
		if (dir != NULL && chdir(dir) == -1)
			exit(1);
		if (bg) {
			close(1); /* close stdout and stderr */
			close(2);
		}
		execvp(argv[0], argv);
		_exit(1);
	default:
		if (!bg) {
			while ((r = waitpid(pid, &status, 0)) == -1 && errno == EINTR)
				continue;
			if (r != -1 && WIFEXITED(status) && WEXITSTATUS(status) != 0)
				r = -1;
		}
	}
	refresh();
	return r;
}

int spawnlp(char *dir, char *file, ...)
{
	va_list ap;
	unsigned int n = 0, i = 0, bg = 0;
	char *argv[32], *args[32], *cmd, *tmp, buf[PATH_MAX];

	strlcpy(buf, file, sizeof(buf));
	va_start(ap, file);
	while (i + 1 < sizeof(args) && (args[i++] = va_arg(ap, char *)));
	args[i] = NULL;
	va_end(ap);
	if ((cmd = strtok(buf, " \t"))) {
		argv[n++] = cmd;
		while (n < sizeof(argv) && (tmp = strtok(NULL, " \t")))
			argv[n++] = tmp;
		if ((bg = (argv[n - 1][0] == '&')))
			n--;
	}
	i = 0;
	while (n + 1 < sizeof(argv) && i < sizeof(args) && args[i])
		argv[n++] = args[i++];
	argv[n] = NULL;
	return spawnvp(dir, argv, bg);
}

int canopendir(char *path)
{
	DIR *d;

	if (!path || (d = opendir(path)) == NULL) {
		xwarn("opendir");
		return 0;
	}
	closedir(d);
	return 1;
}

int xmkdir(char *path)
{
	char *s, tmp[PATH_MAX];

	strlcpy(tmp, path, sizeof(tmp));
	for (s = tmp + 1; *s; s++) {
		if (*s == '/') {
			*s = '\0';
			if (!confirm(tmp, 1, "replace %s with directory? [y/N]:", tmp)) return -1;
			if (mkdir(tmp, 0755) && errno != EEXIST) return -1;
			*s = '/';
		}
	}
	if (!confirm(path, 1, "replace %s with directory? [y/N]:", path)) return -1;
	if (mkdir(path, 0755) && errno != EEXIST) return -1;
	return 0;
}

int touch(char *path)
{
	int fd;
	char *s, tmp[PATH_MAX];

	if (!*path) return 0;
	strlcpy(tmp, path, sizeof(tmp));
	if ((s = strrchr(tmp, '/')) != NULL) {
		*s = '\0';
		if (xmkdir(tmp)) return -1;
		*s = '/';
	}
	if ((fd = open(tmp, O_RDWR|O_CREAT, 0666)) == -1) return -1;
	return close(fd);
}

int mv(char *path, char *src, char *dst)
{
	char *s, new[PATH_MAX], old[PATH_MAX];

	mkpath(path, dst, new, sizeof(new));
	if (!confirm(new, 0, "replace %s? [y/N]:", new)) return -1;
	mkpath(path, src, old, sizeof(old));
	if ((s = strrchr(new, '/')) != NULL) {
		*s = '\0';
		if (xmkdir(new)) return -1;
		*s = '/';
	}
	info("moving: %s -> %s", old, new);
	refresh();
	return rename(old, new);
}

int ln(char *src, char *dst, struct stat *s)
{
	char buf[PATH_MAX];

	if (!confirm(dst, 0, "replace %s with link? [y/N]:", dst)) return 0;
	if (S_ISLNK(s->st_mode)) {
		ssize_t r;
		if ((r = readlink(src, buf, sizeof(buf) - 1)) < 0) return -1;
		buf[r] = '\0';
	} else {
		strlcpy(buf, src, sizeof(buf));
	}
	info("linking: %s -> %s", buf, dst);
	refresh();
	return symlink(buf, dst);
}

int rm(char *src, __attribute__((unused)) char *dst, struct stat *s)
{
	info("removing: %s", src);
	refresh();
	return S_ISDIR(s->st_mode) ? rmdir(src) : unlink(src);
}

int cp(char *src, char *dst, struct stat *s)
{
	int e, sfd, dfd;

	if (S_ISLNK(s->st_mode)) return ln(src, dst, s);
	if (S_ISDIR(s->st_mode)) return xmkdir(dst);
	if (!confirm(dst, 0, "replace %s? [y/N]:", dst)) return 0;
	if ((sfd = open(src, O_RDONLY)) < 0) return -1;
	if ((dfd = open(dst, O_WRONLY|O_CREAT|O_TRUNC, s->st_mode)) < 0) goto error;
	info("copying: %s", src);
	refresh();

#if defined(USE_LOADING) || !defined(__linux__)
	ssize_t nread;
	size_t size = (s->st_size >= 2048000) ? 2048000 : 4096;
	char *buf = xmalloc(size);
#ifdef USE_LOADING
	int i = 0, percent = 0, len = MIN(COLS, (int)sizeof(msg));
	strlcpy(msg, "copying: ", len);
	strlcat(msg, src, len);
	int msglen = strlen(msg);
	int inc = (s->st_size / size) / 100;
	while ((nread = read(sfd, buf, size)) > 0) {
		if (write(dfd, buf, nread) != nread) { free(buf); goto error; }
		if (++i == inc) {
			int done = ++percent * (double)(COLS / 100.0);
			mvprintw(LINES - 1, 0, "%*c", COLS, ' ');
			attron(A_REVERSE);
			for (int j = 0; j < msglen; ) {
				mvprintw(LINES - 1, j, "%c", msg[j]);
				if (++j == done) attroff(A_REVERSE);
			}
			if (done > msglen) mvprintw(LINES - 1, msglen, "%*c", done - msglen, ' ');
			attroff(A_REVERSE);
			refresh();
			i = 0;
		}
	}
#else
	while ((nread = read(sfd, buf, size)) > 0)
		if (write(dfd, buf, nread) != nread) { free(buf); goto error; }
#endif /* USE_LOADING */
	free(buf);
#else
	if (sendfile(dfd, sfd, 0, s->st_size) < 0) goto error;
#endif /* defined(USE_LOADING) || !defined(__linux__) */

	if (fchmod(dfd, s->st_mode) < 0 || (dfd = close(dfd)) < 0 || close(sfd) < 0)
		goto error;
	return 0;

error:
	e = errno;
	close(sfd);
	if (dfd >= 0) close(dfd);
	unlink(dst);
	errno = e;
	return -1;
}

int fsrec_i(recursor *r)
{
	int err;
	DIR *dir;
	struct stat st;
	struct dirent *e;

	if ((dir = opendir(r->src))) {
		int i = strlen(r->src);
		char s[PATH_MAX], d[PATH_MAX];
		strlcpy(s, r->src, sizeof(s));
		strlcpy(d, r->dst, sizeof(d));
		while ((e = readdir(dir))) {
			if (DOT(e->d_name)) continue;
			s[i] = '/';
			s[i + 1] = '\0';
			strlcat(s, e->d_name, sizeof(s));
			strlcat(d, s + r->slen, sizeof(d));
			if (lstat(s, &st)) goto error;
			if (r->type == BREADTH && r->fn(s, d, &st)) goto error;
			if (S_ISDIR(st.st_mode)) {
				r->src = s;
				r->depth += 1;
				if (fsrec_i(r)) goto error;
			}
			if (r->type == DEPTH && r->fn(s, d, &st)) goto error;
			d[r->dlen] = s[i] = '\0';
		}
		closedir(dir);
	}
	return 0;

error:
	err = errno;
	closedir(dir);
	errno = err;
	return -1;
}

int fsrec(recursor *r)
{
	struct stat st;
	char s[PATH_MAX], d[PATH_MAX];

	strlcpy(s, r->src, sizeof(s));
	strlcpy(d, r->dst, sizeof(d));
	if (r->type == BREADTH) {
		strlcat(d, s + r->slen, sizeof(d));
		if (lstat(s, &st) || r->fn(s, d, &st)) return -1;
		d[r->dlen] = '\0';
		fsrec_i(r);
	} else {
		fsrec_i(r);
		strlcat(d, s + r->slen, sizeof(d));
		if (lstat(s, &st) || r->fn(s, d, &st)) return -1;
	}
	return 0;
}

int xrename(char *path, char *editor, int cur)
{
	FILE *f;
	ssize_t n;
	size_t len = PATH_MAX;
	int i, j = 0, err = 0, fd = -1;
	char m[nmarked + 1][PATH_MAX];
	char tmpf[sizeof(tempfile) + 1];
	char *line, *argv[] = { editor, tmpf, NULL };

	if (nmarked == 1) {
		for (i = 0; i < ndents; i++)
			if (dents[i].marked) {
				nmarked = dents[i].marked = 0;
				if ((line = readln("rename: ", dents[i].name)) && strcoll(dents[i].name, line)) {
					if (mv(path, dents[i].name, line)) return -1;
					strlcpy(dents[i].name, line, sizeof(dents[i].name));
				}
				break;
			}
		return 0;
	}

	line = xmalloc(len);
	strlcpy(tmpf, tempfile, sizeof(tmpf));
	if ((fd = mkstemp(tmpf)) < 0 || (f = fdopen(fd, "w")) == NULL) {
		free(line);
		return -1;
	}
	for (i = 0; i < ndents && j < nmarked; i++)
		if (dents[i].marked) {
			dents[i].marked = 0;
			fprintf(f, "%s\n", dents[i].name);
			strlcpy(m[j], dents[i].name, sizeof(m[j]));
			j++;
		}
	nmarked = 0;
	fclose(f);
	if (spawnvp(path, argv, 0) < 0 || (f = fopen(tmpf, "r")) == NULL) {
		free(line);
		return -1;
	}
	for (i = 0; i < j && (n = getline(&line, &len, f)) != -1; i++) {
		line[n] = '\0';
		if (line[n - 1] == '\n') line[--n] = '\0';
		if (i == cur) strlcpy(hist[0], line, sizeof(hist[0]));
		if (!strcoll(m[i], line) || !mv(path, m[i], line)) continue;
		err = errno;
		break;
	}
	free(line);
	fclose(f);
	if (fd >= 0) unlink(tmpf);
	if (err) { errno = err; return -1; }
	return 0;
}

int walksdents(char *path, int action)
{
	struct stat s;
	char src[PATH_MAX], dst[PATH_MAX];
	int i, j = strlen(path), l = strlen(sdpath);

	switch (action) {
	case SEL_CP:
		for (i = 0; i < nsdents; i++) {
			mkpath(sdpath, sdents[i].name, src, sizeof(src));
			if (fsrec(&(recursor){ src, path, l, j, 0, BREADTH, cp })) return -1;
		}
		break;
	case SEL_MV:
		for (i = 0; i < nsdents; i++) {
			mkpath(sdpath, sdents[i].name, src, sizeof(src));
			mkpath(path,   sdents[i].name, dst, sizeof(dst));
			if (rename(src, dst)) {
				if (errno != EXDEV) return -1;
				if (fsrec(&(recursor){ src, path, l, j, 0, BREADTH, cp })) return -1;
				if (fsrec(&(recursor){ src, path, l, j, 0, DEPTH,   rm })) return -1;
			}
		}
		break;
	case SEL_LN:
		for (i = 0; i < nsdents; i++) {
			mkpath(sdpath, sdents[i].name, src, sizeof(src));
			mkpath(path,   sdents[i].name, dst, sizeof(dst));
			if (lstat(src, &s) || ln(src, dst, &s)) return -1;
		}
		break;
	}
	return 0;
}

int setfilter(regex_t *regex, char *filter)
{
	int r;

	if ((r = regcomp(regex, filter, REG_NOSUB|REG_EXTENDED|REG_ICASE)) != 0) {
		char errbuf[PATH_MAX];
		size_t len = COLS > (int)sizeof(errbuf) ? (int)sizeof(errbuf) : COLS;
		regerror(r, regex, errbuf, len);
		info("%s", errbuf);
	}
	return r;
}

int dircmp(mode_t a, mode_t b)
{
	if ( S_ISDIR(a) &&  S_ISDIR(b)) return 0;
	if (!S_ISDIR(a) && !S_ISDIR(b)) return 0;
	if (S_ISDIR(a)) return -1;
	return 1;
}

int entrycmp(const void *va, const void *vb)
{
	int d;
	long long i;
	const entry *a = va, *b = vb;

	if (conf[SEL_DSORT] && (d = dircmp(a->mode, b->mode)) != 0)
		return (i = d) > 0 ? 1 : i < 0 ? -1 : 0;
	if (conf[SEL_MTIME])
		return (i = b->mtime - a->mtime) > 0 ? 1 : i < 0 ? -1 : 0;
	if (conf[SEL_SIZE])
		return (i = b->size - a->size) > 0 ? 1 : i < 0 ? -1 : 0;

	const char *s1 = a->name, *s2 = b->name;
	if (conf[SEL_VERS])
		i = strvcmp(*s1 == '.' ? s1 + 1 : s1, *s2 == '.' ? s2 + 1 : s2);
	else
		i = strcasecmp(*s1 == '.' ? s1 + 1 : s1, *s2 == '.' ? s2 + 1 : s2);
	return i > 0 ? 1 : i < 0 ? -1 : 0;
}

int nextsel(char **run, char **env)
{
	int c;
	static unsigned long idle = 0;

	if ((c = xgetch(NULL)) == KEY_ESC)
		c = META(xgetch(NULL));

	if (c == ERR) {
		idle++;

#if defined(__linux__)
		if (watch >= 0 && ISODD(idle)) {
			char buf[PATH_MAX]__attribute__ ((aligned(__alignof__(struct inotify_event))));
			ssize_t i = read(watcher, buf, sizeof(buf));
			if (i == -1 && errno != EAGAIN) fatal("read");
			if (i > 0) {
				struct inotify_event *e;
				for (char *p = buf; p < buf + i; p += sizeof(struct inotify_event) + e->len) {
					e = (struct inotify_event *)p;
					if (e->mask & (WATCHMASK)) { idle = 0; return SEL_REDRAW; }
				}
			}
		}
#elif defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__) || defined(__APPLE__)
		if (watch >= 0 && ISODD(idle)) {
			struct kevent d;
			if (kevent(watcher, &kev, 1, &d, 1, &kevt) > 0) { idle = 0; return SEL_REDRAW; }
		}
#else
		if (idle >= 10) { idle = 0; return SEL_REDRAW; }
#endif

	} else {
		idle = 0;
		for (unsigned int i = 0; i < LEN(keybinds); i++)
			if (c == keybinds[i].sym) {
				*run = keybinds[i].run;
				*env = keybinds[i].env;
				return keybinds[i].act;
			}
	}
	return 0;
}

char *entsize(entry *ent)
{
	static char out[NAME_MAX];

	*out = '\0';
	if (conf[SEL_SSIZE]) {
		if (ent->size < 1000) { /* <1 KB */
			snprintf(out, sizeof(out), "%lld B", ent->size); // NOLINT
		} else if (ent->size < 1024000) { /* <1 MB */
			snprintf(out, sizeof(out), "%.2f KB", ent->size / 1024.0); // NOLINT
		} else if (ent->size < 1024000000) { /* <1 GB */
			snprintf(out, sizeof(out), "%.2f MB", ent->size / 1024 / 1000.0); // NOLINT
		} else if (ent->size < 1024000000000) { /* <1 TB ( >= 1 TB size files? ) */
			snprintf(out, sizeof(out), "%.2f GB", ent->size / 1024 / 1000 / 1000.0); // NOLINT
		} else { /* >1 TB */
			snprintf(out, sizeof(out), "%.2f TB", ent->size / 1024 / 1000 / 1000 / 1000.0); // NOLINT
		}
	}
	return out;
}

void printent(char *path, entry *ent, int active, int line)
{
	char *size;
	struct stat s;
	unsigned int i;
	char lnk[PATH_MAX + 4], ich[4], iich[4];
	int r, len, yank = 0, max = COLS - 2, attr = 0, nlen;
	char name[LINE_MAX], itmp[PATH_MAX], buf[PATH_MAX];

	if (!ent || !ent->name[0]) return;
	lnk[0] = ich[0] = iich[0] = '\0';
	nlen = strlen(ent->name);
	len = nlen < max ? nlen + 1 : max;
	strlcpy(name, ent->name, len);
	for (i = 0; i < LEN(filetypes); i++)
		if ((ent->mode & S_IFMT) == filetypes[i].ftmask
				|| (filetypes[i].ftmask == S_IXUSR && (ent->mode & filetypes[i].ftmask)))
		{
			mkpath(path, name, itmp, sizeof(itmp));
			strlcpy(ich, (char *)&filetypes[i].idchar, sizeof(ich));
			len++;
			if (filetypes[i].ftmask != S_IFLNK) {
				attr |= filetypes[i].colourmask;
			} else if (stat(itmp, &s) == -1 && errno == ENOENT) {
				attr |= A_NORMAL | COLOR_PAIR(1);
			} else if ((r = readlink(itmp, buf, sizeof(buf) - 1)) >= 0) {
				if (S_ISDIR(s.st_mode)) {
					strlcat(ich, "/", sizeof(ich));
					len++;
				}
				attr |= filetypes[i].colourmask;
				buf[r] = '\0';
				len += strlen(buf) + 4;
				strlcpy(lnk, " -> ", sizeof(lnk));
				strlcat(lnk, buf, sizeof(lnk));
				if (lstat(buf, &s) != -1 && S_ISDIR(s.st_mode)) {
					strlcat(iich, "/", sizeof(iich));
					len++;
				}
			}
			break;
		}
	if (*sdpath && !strcoll(path, sdpath)) {
		mkpath(path, ent->name, itmp, sizeof(itmp));
		for (int j = 0; j < nsdents; j++) {
			char tmp[PATH_MAX];
			mkpath(sdpath, sdents[j].name, tmp, sizeof(tmp));
			if (!strcoll(tmp, itmp)) {
				yank = 1;
				len += strlen(yanksym);
				break;
			}
		}
	}

	size = entsize(ent);
	if (ent->marked) {
		attr = A_REVERSE;
		attron(attr);
	}

	mvprintw(line, 0, "%s%s", active ? cursor : nocursor, yank ? yanksym : "");
	attron(attr);
	printw("%s", name);
	attroff(ent->marked ? 0 : attr);
	printw("%s", ich);
	attron(attr);
	printw("%s", lnk);
	attroff(ent->marked ? 0 : attr);
	printw("%s%*c", iich, longest > 0 ? longest - len : len, ' ');
	attroff(ent->marked ? 0 : attr);
	printw("%s%*c", size, (int)(COLS - getcurx(stdscr) - strlen(size)), ' ');
	attroff(attr);
}

int dentfill(char *path, regex_t *re)
{
	DIR *dirp;
	int n = 0;
	struct stat s, sb;

	longest = 0;
	if ((dirp = opendir(path))) {
		struct dirent *dp;
		char newpath[PATH_MAX], buf[PATH_MAX];
		while ((dp = readdir(dirp))) {
			if (DOT(dp->d_name) || regexec(re, dp->d_name, 0, NULL, 0)) continue;
			if (!(dents = xrealloc(dents, (n + 1) * sizeof(*dents)))) continue;
			strlcpy(dents[n].name, dp->d_name, sizeof(dents[n].name));
			mkpath(path, dp->d_name, newpath, sizeof(newpath));
			if (lstat(newpath, &sb) == -1) fatal("lstat");
			int i = 0;
			if (conf[SEL_SSIZE] && (i = mbstowcs(NULL, dp->d_name, 0))) {
				int r;
				if ((sb.st_mode & S_IFMT) == S_IFLNK && stat(newpath, &s) != -1
						&& (r = readlink(newpath, buf, sizeof(buf) - 1)) >= 0)
				{
					buf[r] = '\0';
					i += strlen(buf) + 6;
				}
				if (i + 4 > longest) longest = i + 4;
			}
			dents[n].mode = sb.st_mode;
			dents[n].size = sb.st_size;
			dents[n].mtime = sb.st_mtime;
			dents[n].marked = 0;
			n++;
		}
		if (closedir(dirp) == -1) fatal("closedir");
	}
	return n;
}

int dentfind(char *path)
{
	int i, ormatch = 0;
	char tmp[PATH_MAX];

	for (i = 0; i < ndents; i++) {
		mkpath(path, dents[i].name, tmp, sizeof(tmp));
		if (!strcoll(tmp, hist[0])) return i;
		if (!strcoll(tmp, hist[1]) || !strcoll(tmp, hist[2])) ormatch = i;
	}
	return ormatch;
}

int populate(char *path, char *fltr)
{
	regex_t re;
	int nsave = ndents;
	entry *save = NULL;

	if (!canopendir(path) || setfilter(&re, fltr)) return -1;
	if (ndents && nmarked) { /* save marked entries */
		if (!(save = xmalloc((ndents + 1) * sizeof(*dents))))
			fatal("unable to allocate enough space");
		memcpy(save, dents, ndents * sizeof(*dents)); // NOLINT
	}
	free(dents);
	dents = NULL;
	ndents = dentfill(path, &re);
	regfree(&re);
	if (ndents) {
		qsort(dents, ndents, sizeof(*dents), entrycmp);
		cur = dentfind(path);
		if (save)
			for (int i = 0; i < nsave; i++) { /* transfer marked entries */
				if (!save[i].marked) continue;
				for (int j = 0; j < ndents; j++)
					if (!strcoll(save[i].name, dents[j].name)) {
						dents[j].marked = 1;
						break;
					}
			}
	}
	free(save);
	return 0;
}

void redraw(char *path)
{
	size_t ncols;
	char cwd[PATH_MAX], cwdresolved[PATH_MAX];
	int i, attr = 0, l = 2, nlines = MIN(LINES - 4, ndents);

	erase();
	for (i = strlen(path) - 1; i > 0 && path[i] == '/'; i--)
		path[i] = '\0';
	if ((ncols = COLS) > PATH_MAX) ncols = PATH_MAX;
	strlcpy(cwd, path, ncols);
	cwd[ncols - 1] = '\0';
	realpath(cwd, cwdresolved);
	if (tildehome && home && !strcoll(home, cwdresolved)) strlcpy(cwdresolved, "~", ncols);
	if (!uid) attr |= A_NORMAL|COLOR_PAIR(1);
	attron(attr);
	mvprintw(0, 1, "%s", cwdresolved);
	attroff(attr);
	mvhline(1, 0, ACS_HLINE, COLS);
	if (cur < nlines / 2)
		for (i = 0; i < nlines; i++)
			printent(path, &dents[i], i == cur, l++);
	else if (cur >= ndents - nlines / 2)
		for (i = ndents - nlines; i < ndents; i++)
			printent(path, &dents[i], i == cur, l++);
	else
		for (i = cur - nlines / 2; i < cur + nlines / 2 + ISODD(nlines); i++)
			printent(path, &dents[i], i == cur, l++);
	if (*msg) info("%s", msg);
	*msg = '\0';
}

void browse(char *ipath, char *ifilter)
{
	FILE *f;
	regex_t re;
	struct stat sb;
	int i, j, sel = -1, key, fd, bg, updwatch = 1;
	char *dir, *cmd, *tmp, *run, *env, *argv[32];
	char path[PATH_MAX], newpath[PATH_MAX], fltr[LINE_MAX], buf[PATH_MAX];

	strlcpy(path, ipath, sizeof(path));
	strlcpy(fltr, ifilter, sizeof(fltr));
	*hist[0] = *hist[1] = *hist[2] = '\0';

#define savehist(i, s1, s2)               \
	updwatch = 1, nmarked = 0;            \
	savecur(path, cur, i, 0);             \
	strlcpy(s1, path, sizeof(s1));        \
	strlcpy(path, s2, sizeof(path));      \
	strlcpy(fltr, ifilter, sizeof(fltr))

#define errmsg(s) snprintf(msg, sizeof(msg), "%s: %s", s, strerror(errno)) /* NOLINT */

begin:
	if (populate(path, fltr) == -1) goto nochange;

	if (updwatch) {
		updwatch = 0;
#if defined(__linux__)
		if (watch >= 0) inotify_rm_watch(watcher, watch);
		watch = inotify_add_watch(watcher, path, WATCHMASK);
#elif defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__) || defined(__APPLE__)
		if (watch >= 0) close(event_fd);

#if defined(O_EVTONLY)
		watch = open(path, O_EVTONLY);
#else
		watch = open(path, O_RDONLY);
#endif
		if (watch >= 0)
			EV_SET(&kev, watch, EVFILT_VNODE, EV_ADD | EV_CLEAR, WATCHMASK, 0, path);
#endif
	}

	for (;;) {
		redraw(path);
nochange:
		switch ((sel = nextsel(&run, &env))) {
		case SEL_QUIT:
			if ((f = fopen(savefile, "w"))) {
				fprintf(f, "%s\n", path);
				fclose(f);
			}
			free(dents);
			free(sdents);
			for (unsigned int p = 0; p < LEN(filerules); p++)
				regfree(&filerules[p].regcomp);
			return;
		case SEL_BACK:
			if (!strcoll(path, "/") || !strcoll(path, ".") || strchr(path, '/') == NULL)
				goto nochange;
			strlcpy(newpath, path, sizeof(newpath)); /* so dirname(3) doesn't mangle path */
			if ((dir = dirname(newpath)) == NULL) fatal("dirname");
			if (!canopendir(dir)) goto nochange;
			savehist(1, hist[0], dir);
			goto begin;
		case SEL_GOIN:
			if (!ndents) goto nochange;
			mkpath(path, dents[cur].name, newpath, sizeof(newpath));
			if ((fd = open(newpath, O_RDONLY|O_NONBLOCK)) == -1) {
				xwarn("open");
				goto nochange;
			} else if (fstat(fd, &sb) == -1) {
				xwarn("fstat");
				close(fd);
				goto nochange;
			}
			close(fd);
			switch (sb.st_mode & S_IFMT) {
			case S_IFDIR:
				if (!canopendir(newpath)) goto nochange;
				updwatch = 1;
				strlcpy(path, newpath, sizeof(path));
				strlcpy(fltr, ifilter, sizeof(fltr));
				nmarked = 0;
				goto begin;
			case S_IFREG:
				for (unsigned int ui = 0, j = 0; ui < LEN(filerules); ui++)
					if (!regexec(&filerules[ui].regcomp, dents[cur].name, 0, NULL, 0)) {
						bg = 0;
						strlcpy(buf, filerules[ui].argv, sizeof(buf));
						if ((cmd = strtok(buf, " \t"))) {
							argv[j++] = cmd;
							while (j + 3 < sizeof(argv) && (tmp = strtok(NULL, " \t")))
								argv[j++] = tmp;
							if ((bg = (argv[j - 1][0] == '&'))) j--;
						}
						argv[j++] = newpath;
						argv[j] = NULL;
						savecur(path, cur, 0, 0);
						if (spawnvp(path, argv, bg) == -1) {
							info("failed to open %s", argv[0]);
							goto nochange;
						}
						goto begin;
					}
				continue;
			default:
				info("unsupported filetype");
				goto nochange;
			}
		case SEL_FLTR:
			if ((tmp = readln("/", NULL)) == NULL) tmp = ifilter;
			if (setfilter(&re, tmp)) goto nochange;
			regfree(&re);
			strlcpy(fltr, tmp, sizeof(fltr));
			savecur(path, cur, 0, 0);
			goto begin;
		case SEL_NEXT:
			if (cur < ndents - 1) cur++;
			break;
		case SEL_PREV:
			if (cur) cur--;
			break;
		case SEL_PGDN:
			if (cur < ndents - 1) cur += MIN((LINES - 4) / 2, ndents - 1 - cur);
			break;
		case SEL_PGUP:
			if (cur) cur -= MIN((LINES - 4) / 2, cur);
			break;
		case SEL_LAST:
			cur = ndents - 1;
			break;
		case SEL_FIRST:
			cur = 0;
			break;
		case SEL_JUMP:
			if ((key = xgetch("jump: ")) == 'g') {
				cur = 0;
				break;
			}
			for (unsigned int ui = 0; ui < LEN(dirjumps); ui++)
				if (key == dirjumps[ui].key) {
					if (dirjumps[ui].path[0] == '~') {
						if (!home) break;
						strlcpy(newpath, home, sizeof(newpath));
						strlcat(newpath, dirjumps[ui].path + 1, sizeof(newpath));
					} else {
						mkpath(path, dirjumps[ui].path, newpath, sizeof(newpath));
					}
					if (!strcoll(path, newpath)) break;
					if (!canopendir(newpath)) goto nochange;
					if (hist[2][0]) strlcpy(hist[0], hist[2], sizeof(hist[0]));
					savehist(2, lastdir, newpath);
					goto begin;
				}
			goto nochange;
		case SEL_UNYANK:
			if (!freesdents()) break;
			goto nochange;
		case SEL_MARK:
			if (!ndents) goto nochange;
			nmarked += (dents[cur].marked = !dents[cur].marked) ? 1 : -1;
#ifdef SEL_ADVANCE
			if (cur < ndents - 1) cur++;
#endif
			break;
		case SEL_YANK:
			if (!ndents) goto nochange;
			if (!nmarked) nmarked = dents[cur].marked = 1;
			savecur(path, cur, 0, 0);
			freesdents();
			strlcpy(sdpath, path, sizeof(sdpath));
			if ((sdents = xmalloc((nmarked + 1) * sizeof(*sdents))))
				for (i = 0; i < ndents && nsdents < nmarked; i++)
					if (dents[i].marked) {
						strlcpy(sdents[nsdents].name, dents[i].name, sizeof(sdents[nsdents].name));
						sdents[nsdents].mode = dents[i].mode;
						dents[i].marked = 0;
						nsdents++;
					}
			nmarked = 0;
			break;
		case SEL_RENAME:
			if (!ndents) goto nochange;
			if (!nmarked) nmarked = dents[cur].marked = 1;
			if (xrename(path, xgetenv(env, run), cur)) errmsg("rename"); // NOLINT
			savecur(path, cur, 0, 0);
			goto begin;
		case SEL_RM:
			if (!ndents || xgetch("remove? [D]: ") != 'D') goto nochange;
			if (!nmarked) nmarked = dents[cur].marked = 1;
			mvprintw(LINES - 1, 0, "%*c", COLS, ' ');
			savecur(path, cur, 0, 1);
			for (i = 0; i < ndents && nmarked; i++)
				if (dents[i].marked) {
					dents[i].marked = 0;
					mkpath(path, dents[i].name, newpath, sizeof(newpath));
					j = strlen(newpath);
					if (fsrec(&(recursor){ newpath, newpath, j, j, 0, DEPTH, rm })) {
						if (errno == ENOTEMPTY) errno = EACCES;
						errmsg("remove");
						break;
					}
					nmarked--;
				}
			goto begin;
		case SEL_LN: /* fallthrough */
		case SEL_CP: /* fallthrough */
		case SEL_MV:
			if (!nsdents || !strcoll(sdpath, path)) goto nochange;
			savecur(path, cur, 0, 0);
			if (walksdents(path, sel)) {
				if (sel == SEL_CP && errno == ENOTEMPTY) errno = EACCES;
				errmsg(sel == SEL_CP ? "copy" : sel == SEL_MV ? "move" : "link");
			} else if (sel == SEL_MV)
				freesdents();
			goto begin;
		case SEL_CD:
			if ((tmp = readln("cd ", NULL)) == NULL) goto nochange;
			mkpath(path, tmp, newpath, sizeof(newpath));
			if (!canopendir(newpath)) goto nochange;
			savehist(2, lastdir, newpath);
			goto begin;
		case SEL_NEW:
			if ((key = xgetch("[f]ile or [d]ir")) != 'd' && key != 'f') goto nochange;
			if (!(tmp = readln(key == 'd' ? "dir name: " : "file name: ", NULL))) goto nochange;
			mkpath(path, tmp, newpath, sizeof(newpath));
			if ((key == 'd' && xmkdir(newpath)) || (key == 'f' && touch(newpath))) {
				errmsg(key == 'd' ? "mkdir" : "touch");
				break;
			}
			char *s, c;
			if (strrchr(tmp, '/') && (s = strrchr(newpath, '/'))) {
				c = *s;
				*s = '\0';
				strlcpy(hist[0], newpath, sizeof(hist[0]));
				*s = c;
			} else {
				strlcpy(hist[0], newpath, sizeof(hist[0]));
			}
			goto begin;
		case SEL_MTIME: /* fallthrough */
		case SEL_DSORT: /* fallthrough */
		case SEL_VERS: /* fallthrough */
		case SEL_SIZE: /* fallthrough */
		case SEL_SSIZE:
			conf[sel] = !conf[sel];
			savecur(path, cur, 0, 0);
			goto begin;
		case SEL_DOTS:
			showhidden ^= 1;
			ifilter = showhidden ? "." : "^[^.]";
			strlcpy(fltr, ifilter, sizeof(fltr));
			savecur(path, cur, 0, 0);
			goto begin;
		case SEL_REDRAW:
			savecur(path, cur, 0, 0);
			goto begin;
		case SEL_RUN:
			savecur(path, cur, 0, 0);
			spawnlp(path, xgetenv(env, run), (void *)0);
			goto begin;
		case SEL_RUNARG:
			savecur(path, cur, 0, 0);
			spawnlp(path, xgetenv(env, run), dents[cur].name, (void *)0);
			goto begin;
		}
	}
#undef savehist
}

void usage()
{
	fprintf(stderr, "usage: %s [-ct] [-f savefile] [dir]\n", argv0);
	exit(1);
}

int main(int argc, char *argv[])
{
	DIR *d;
	char cwd[PATH_MAX];
	char *ifilter, *ipath;

	ARGBEGIN {
		case 'c': usecolour = 1; break;
		case 't': tildehome = 1; break;
		case 'f': savefile = EARGF(usage()); break;
		default: usage(); break;
	} ARGEND

	if (argc > 1) usage();
	if (!isatty(0) || !isatty(1)) {
		fprintf(stderr, "noice: stdin or stdout is not a tty\n");
		return EXIT_FAILURE;
	}
	if ((uid = getuid()) == 0) showhidden = 1;
	ifilter = showhidden ? "." : "^[^.]";
	ipath = argv[0]
		? (strncmp(argv[0], "file://", 7) ? argv[0] : &argv[0][7])
		: (getcwd(cwd, sizeof(cwd)) ? cwd : "/");
	signal(SIGINT, SIG_IGN);

	if (!ipath || (d = opendir(ipath)) == NULL) {
		fprintf(stderr, "noice: %s: %s\n", ipath, strerror(errno));
		return EXIT_FAILURE;
	}
	closedir(d);

	if (!(home = getenv("HOME")))
		fprintf(stderr, "noice: unable to get HOME value from env\n");

#if defined(__linux__)
	if ((watcher = inotify_init1(IN_NONBLOCK)) < 0) {
		fprintf(stderr, "noice: inotify: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}
	watch = -1;
#elif defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__) || defined(__APPLE__)
	if ((watcher = kqueue()) < 0) {
		fprintf(stderr, "noice: kqueue: %s\n", strerror(errno));
		return EXIT_FAILURE;
	}
#endif

	conf[SEL_DSORT] = dirsort;
	conf[SEL_SSIZE] = showsize;
	conf[SEL_MTIME] = timesort;
	conf[SEL_SIZE]  = sizesort;
	conf[SEL_VERS]  = verssort;
	initrules();
	setlocale(LC_ALL, "");
	initcurses();
	browse(ipath, ifilter);
	endwin();

	if (watch >= 0) {
#if defined(__linux__)
		inotify_rm_watch(watcher, watch);
#elif defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__) || defined(__APPLE__)
		close(watch);
#endif
	}
	close(watcher);

	return EXIT_SUCCESS;
}
